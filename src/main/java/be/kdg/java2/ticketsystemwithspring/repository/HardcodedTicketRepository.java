package be.kdg.java2.ticketsystemwithspring.repository;

import be.kdg.java2.ticketsystemwithspring.domain.HardwareTicket;
import be.kdg.java2.ticketsystemwithspring.domain.Ticket;
import be.kdg.java2.ticketsystemwithspring.domain.TicketResponse;
import be.kdg.java2.ticketsystemwithspring.domain.TicketState;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class HardcodedTicketRepository implements TicketRepository {
    private static List<Ticket> tickets = new ArrayList<>();
    private static List<TicketResponse> responses = new ArrayList<>();

    static {
        seed();
    }

    private static void seed(){
        Ticket t1 = new Ticket();
        t1.setTicketNumber(tickets.size() + 1); // Ok, no ticket willbedeleted
        t1.setAccountId(1);
        t1.setText("Can't logon to the webmail");
        t1.setDateOpened(LocalDateTime.of(2017, 9, 9, 13, 5, 59));
        t1.setState(TicketState.OPEN);
        t1.setResponses(new ArrayList<>());
        tickets.add(t1);
        TicketResponse t1r1 = new TicketResponse();
        t1r1.setId(responses.size() + 1);
        t1r1.setTicket(t1);
        t1r1.setText("Account was blocked");
        t1r1.setDate(LocalDateTime.of(2017, 9, 9, 13, 24, 48));
        t1r1.setClientResponse(false);
        t1.getResponses().add(t1r1);
        responses.add(t1r1);
        TicketResponse t1r2 = new TicketResponse();
        t1r2.setId(responses.size() + 1);
        t1r2.setTicket(t1);
        t1r2.setText("Account back ok, set to new password");
        t1r2.setDate(LocalDateTime.of(2017, 9, 9, 13, 29, 11));
        t1r2.setClientResponse(true);
        t1.getResponses().add(t1r2);
        responses.add(t1r2);
        TicketResponse t1r3 = new TicketResponse();
        t1r2.setId(responses.size() + 1);
        t1r2.setTicket(t1);
        t1r2.setText("Logon worked, password changed");
        t1r2.setDate(LocalDateTime.of(2017, 9, 10, 7, 22, 36));
        t1r2.setClientResponse(true);
        t1.getResponses().add(t1r3);
        responses.add(t1r3);
        t1.setState(TicketState.CLOSED);
        Ticket t2 = new Ticket();
        t2.setTicketNumber(tickets.size() + 1);
        t2.setAccountId(1);
        t2.setText("No internet connection");
        t2.setDateOpened(LocalDateTime.of(2017, 11, 5, 9, 45, 13));
        t2.setState(TicketState.OPEN);
        t2.setResponses(new ArrayList<>());
        tickets.add(t2);
        TicketResponse t2r1 = new TicketResponse();
        t1r2.setId(responses.size() + 1);
        t1r2.setTicket(t2);
        t1r2.setText("Check if cable is ok");
        t1r2.setDate(LocalDateTime.of(2017, 11, 5, 11, 25, 42));
        t1r2.setClientResponse(false);
        t2.getResponses().add(t2r1);
        responses.add(t2r1);
        t2.setState(TicketState.ANSWERED);
        HardwareTicket ht1 = new HardwareTicket();
        ht1.setTicketNumber(tickets.size() + 1);
        ht1.setAccountId(2);
        ht1.setText("Blue screen!");
        ht1.setDateOpened(LocalDateTime.of(2017, 12, 14, 19, 5, 2));
        ht1.setState(TicketState.OPEN);
        ht1.setDeviceName("PC-123456");
        tickets.add(ht1);
        //print all tickets:
        System.out.println("All tickets:");
        tickets.forEach(System.out::println);
        //print all responses to ticket 1:
        System.out.println("All responses to ticket 1:");
        responses.stream().filter(response -> {
                    Ticket ticket = response.getTicket();
                    if (ticket == null) return false;
                    else return ticket.getTicketNumber() == 1;
                })
                .forEach(System.out::println);
    }

    //Use Create - Read - Update - Delete naming for the methods in
    //the repository (CRUD)
    @Override
    public List<Ticket> readTickets() {
        return tickets;
    }

    @Override
    public Ticket createTicket(Ticket ticket){
       ticket.setTicketNumber(tickets.size() + 1);
       tickets.add(ticket);
       return ticket;
    }
}
