package be.kdg.java2.ticketsystemwithspring;

import be.kdg.java2.ticketsystemwithspring.presentation.View;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TicketsystemwithspringApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                SpringApplication.run(TicketsystemwithspringApplication.class, args);
        //View view = (View) context.getBean("view");
        View view = context.getBean(View.class);
        view.showMenu();
        context.close();
    }
}
