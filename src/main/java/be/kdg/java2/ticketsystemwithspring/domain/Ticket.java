package be.kdg.java2.ticketsystemwithspring.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Ticket {
    private int accountId;
    private LocalDateTime dateOpened;
    private String text;
    private int ticketNumber;
    //relations:
    private List<TicketResponse> responses;
    private TicketState state;

    public int getAccountId() {
        return accountId;
    }

    public LocalDateTime getDateOpened() {
        return dateOpened;
    }

    public String getText() {
        return text;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public List<TicketResponse> getResponses() {
        return responses;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public void setDateOpened(LocalDateTime dateOpened) {
        this.dateOpened = dateOpened;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public void setResponses(List<TicketResponse> responses) {
        this.responses = responses;
    }

    public void setState(TicketState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "accountId=" + accountId +
                ", dateOpened=" + dateOpened +
                ", text='" + text + '\'' +
                ", ticketNumber=" + ticketNumber +
                ", state=" + state +
                '}';
    }
}
