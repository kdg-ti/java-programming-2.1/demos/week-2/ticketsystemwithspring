package be.kdg.java2.ticketsystemwithspring.domain;

public enum TicketState {
    OPEN, ANSWERED, CLIENTANSWER, CLOSED
}
