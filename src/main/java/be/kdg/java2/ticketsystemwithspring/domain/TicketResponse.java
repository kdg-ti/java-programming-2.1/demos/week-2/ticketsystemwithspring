package be.kdg.java2.ticketsystemwithspring.domain;

import java.time.LocalDateTime;

public class TicketResponse {
    private LocalDateTime date;
    private int id;
    private boolean isClientResponse;
    private String text;
    //Relation to ticket:
    private Ticket ticket;

    public LocalDateTime getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public boolean isClientResponse() {
        return isClientResponse;
    }

    public String getText() {
        return text;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setClientResponse(boolean clientResponse) {
        isClientResponse = clientResponse;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    @Override
    public String toString() {
        return "TicketResponse{" +
                "date=" + date +
                ", id=" + id +
                ", isClientResponse=" + isClientResponse +
                ", text='" + text + '\'' +
                ", ticket=" + ticket +
                '}';
    }
}
