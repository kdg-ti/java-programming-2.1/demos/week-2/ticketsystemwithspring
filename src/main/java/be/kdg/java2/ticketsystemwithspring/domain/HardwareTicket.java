package be.kdg.java2.ticketsystemwithspring.domain;

public class HardwareTicket extends Ticket{
    private String deviceName;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public String toString() {
        return "HardwareTicket{" +
                "deviceName='" + deviceName + '\'' +
                '}' + super.toString();
    }
}
