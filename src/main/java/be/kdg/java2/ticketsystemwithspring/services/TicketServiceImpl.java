package be.kdg.java2.ticketsystemwithspring.services;

import be.kdg.java2.ticketsystemwithspring.domain.HardwareTicket;
import be.kdg.java2.ticketsystemwithspring.domain.Ticket;
import be.kdg.java2.ticketsystemwithspring.domain.TicketState;
import be.kdg.java2.ticketsystemwithspring.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Component
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    //In the service layer we do not use the CRUD naming
    @Override
    public Ticket addTicket(int accountId, String question) {
        Ticket ticket = new Ticket();
        ticket.setAccountId(accountId);
        ticket.setDateOpened(LocalDateTime.now());
        ticket.setState(TicketState.OPEN);
        ticket.setText(question);
        return ticketRepository.createTicket(ticket);
    }

    @Override
    public Ticket addTicket(int accountId, String devicename, String question) {
        HardwareTicket ticket = new HardwareTicket();
        ticket.setDeviceName(devicename);
        ticket.setAccountId(accountId);
        ticket.setDateOpened(LocalDateTime.now());
        ticket.setState(TicketState.OPEN);
        ticket.setText(question);
        return ticketRepository.createTicket(ticket);
    }

    @Override
    public List<Ticket> getAllTickets(){
        return ticketRepository.readTickets();
    }
}
