package be.kdg.java2.ticketsystemwithspring.services;

import be.kdg.java2.ticketsystemwithspring.domain.Ticket;

import java.util.List;

public interface TicketService {
    //In the service layer we do not use the CRUD naming
    Ticket addTicket(int accountId, String question);
    Ticket addTicket(int accountId, String devicename, String question);
    List<Ticket> getAllTickets();
}
